package com.lithoser.common.util;

public enum YesNoType {
	Yes("Y"), No("N");

	private String value;

	private YesNoType(String value) {
		this.value = value;
	}

	public String getValue() {
		return value;
	}
}
