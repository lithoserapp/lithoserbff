package com.lithoser.common.util;

public enum ProfileType {
	Vendor("V"), Technician("T"), Customer("C");

	private String value;

	private ProfileType(String value) {
		this.value = value;
	}

	public String getValue() {
		return value;
	}
}
