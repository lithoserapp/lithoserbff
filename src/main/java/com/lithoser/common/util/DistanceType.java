package com.lithoser.common.util;

public enum DistanceType {
	Miles,
    KiloMeters
}
