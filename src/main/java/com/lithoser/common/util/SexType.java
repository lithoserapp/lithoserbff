package com.lithoser.common.util;

public enum SexType {
	Male("M"), Female("F"), Other("O");

	private String value;

	private SexType(String value) {
		this.value = value;
	}

	public String getValue() {
		return value;
	}
}
