package com.lithoser.common.util;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
/**
 * 
 * @author Sanjay
 *
 */
public class LogUtil
{
    /** Log Object for the Project. */
    public static final Logger LOG = LogManager.getLogger("com.lithoser.common");
}