package com.lithoser.common.util;

import java.io.Serializable;

/**
 * 
 * @author Sanjay
 *
 */
public class ApplicationConfig implements Serializable{

	private static final long serialVersionUID = -1;
	
	private double earthRadiusInKM;
	private double earthRadiusInMiles;
	private double cityLimitsInMiles;
	
	public double getEarthRadiusInKM() {
		return earthRadiusInKM;
	}
	public void setEarthRadiusInKM(double earthRadiusInKM) {
		this.earthRadiusInKM = earthRadiusInKM;
	}
	public double getEarthRadiusInMiles() {
		return earthRadiusInMiles;
	}
	public void setEarthRadiusInMiles(double earthRadiusInMiles) {
		this.earthRadiusInMiles = earthRadiusInMiles;
	}
	public double getCityLimitsInMiles() {
		return cityLimitsInMiles;
	}
	public void setCityLimitsInMiles(double cityLimitsInMiles) {
		this.cityLimitsInMiles = cityLimitsInMiles;
	}	
	
}