package com.lithoser.common.util;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.lang.Number;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.web.client.RestTemplate;
import com.google.gson.Gson;
import com.google.i18n.phonenumbers.NumberParseException;
import com.google.i18n.phonenumbers.NumberParseException.ErrorType;
import com.google.i18n.phonenumbers.PhoneNumberUtil;
import com.google.i18n.phonenumbers.PhoneNumberUtil.PhoneNumberFormat;
import com.google.i18n.phonenumbers.Phonenumber.PhoneNumber;

/**
 * 
 * @author Sanjay
 *
 */
public class LithoserUtil{

	public static String TECH_ERROR = "Sorry, an unexpected error has occurred. "
			+ "Please write to admin@lithoserapp.com to report this.";
	//Constants
	private static ApplicationContext context = null;	
	private static Gson gson = null;
	private static RestTemplate restTemplate = null;
	
	public static double EARTH_RADIUS_IN_KM;
	public static double EARTH_RADIUS_IN_MILES;
	public static double CITY_LIMITS_IN_MILES;
	
	static{
		ApplicationContext appContext = getApplicationContext();
	    ApplicationConfig config = (ApplicationConfig)appContext.getBean("appConfig");
	    EARTH_RADIUS_IN_KM = config.getEarthRadiusInKM();
	    EARTH_RADIUS_IN_MILES = config.getEarthRadiusInMiles();
	    CITY_LIMITS_IN_MILES = config.getCityLimitsInMiles();
	}
	public static ApplicationContext getApplicationContext(){
		if(context == null){
			context = new ClassPathXmlApplicationContext("spring-config.xml");
		}
		return context;
	}
	/**
	 * 
	 * @param statement
	 * @param rs
	 * @param conn
	 * @param caller
	 */
	public static void cleanUpSQLData(Statement statement, ResultSet rs, Connection conn, Object caller) {

        //Take care of cleaning up the sql objects!
        try {
            if (rs != null) {
                rs.close();
            }
        }
        catch (Exception sqe) {
            sqe.printStackTrace();
        }
        try {
            if (statement != null) {
                statement.close();
            }
        }
        catch (Exception sqe) {
        	sqe.printStackTrace();
        }
        
        try {
            if (conn != null) {
                conn.close();
            }
        }
        catch (Exception sqe) {
        	sqe.printStackTrace();
        }
    }
	/**
	 * 
	 * @param statement
	 * @param rs
	 * @param caller
	 */
	public static void cleanUpSQLData(Statement statement, ResultSet rs,  Object caller) {

        //Take care of cleaning up the sql objects!
        try {
            if (rs != null) {
                rs.close();
            }
        }
        catch (Exception sqe) {
            sqe.printStackTrace();
        }
        try {
            if (statement != null) {
                statement.close();
            }
        }
        catch (Exception sqe) {
        	sqe.printStackTrace();
        }

    }

	/**
	 * 
	 * @param contactNumber
	 * @return
	 */
	
	public static String getContactNumber(String contactNumber){
		return LithoserUtil.getContactNumber(contactNumber,"US");
	}
	/**
	 * 
	 * @param contactNumber
	 * @return
	 */
	public static String getContactNumberRegion(String contactNumber){
		String regionCode = ""; 
		PhoneNumberUtil phoneNumberUtil = PhoneNumberUtil.getInstance();
		PhoneNumber number ;
		 try {
		        number = phoneNumberUtil.parse(contactNumber, null);
		        regionCode = phoneNumberUtil.getRegionCodeForNumber(number);
		 }catch (NumberParseException e) {
		    	//e.getStackTrace();		    	
		 }finally{
			 if(regionCode == null || regionCode.isEmpty()){
				 regionCode = "US";// by default, it will take USA international code
			 }
		 }
		 return regionCode;
	}
	/**
	 * 
	 * @param contactNumber
	 * @param region
	 * @return
	 */
	public static String getContactNumber(String contactNumber, String region){

		PhoneNumberUtil phoneNumberUtil = PhoneNumberUtil.getInstance();
		String decodedNumber = null;
		PhoneNumber number;
		    try {
		        number = phoneNumberUtil.parse(contactNumber, region);// by default, it will take USA international code
		        decodedNumber = phoneNumberUtil.format(number, PhoneNumberFormat.E164);
		    } catch (NumberParseException e) {
		    	//e.getStackTrace();
		    	if(e.getErrorType() == ErrorType.INVALID_COUNTRY_CODE){
		    		decodedNumber = "1" + contactNumber;// Just to make sure it takes USA international code
		    	}
		    	else if(e.getErrorType() == ErrorType.NOT_A_NUMBER ){
		    		return "";
		    	}
		    	else if(e.getErrorType() == ErrorType.TOO_LONG || 
		    			e.getErrorType() == ErrorType.TOO_SHORT_AFTER_IDD || 
		    			e.getErrorType() == ErrorType.TOO_SHORT_NSN){
		    		decodedNumber = contactNumber;
		    	}
		    }
		    decodedNumber = decodedNumber.replaceAll("\\D+",""); //first remove if it there and then add '+'
		    decodedNumber = "+" + decodedNumber;
		return decodedNumber;
	}
	

	/**
	 * 
	 * @param str
	 * @return
	 */
	public static boolean isNumber(String str){
		return NumberUtils.isNumber(str);
	}
	
	/**
	 * Removes diacritics (~= accents) from a string. The case will not be altered.
	   For instance, '�' will be replaced by 'a'.
	 * @param str
	 * @return
	 */
	public static String stripAccents(String str){
		return StringUtils.stripAccents(str);
	}

	/**
	 * Removes the duplicate in the array and join them back
	 * @param items
	 * @return String
	 */
	public static String removeDuplicates(String[] items){
		List<String> list = Arrays.asList(items);
		return StringUtils.join(removeDuplicates(list).iterator(),",");
	}
	
	/**
	 *  Removes the duplicate in a list
	 * @param list
	 * @return set
	 */
	public static Set<String> removeDuplicates(List<String> list) {

		// Record encountered Strings in HashSet.
		Set<String> set = new HashSet<String>();

		// Loop over argument list.
		for (String item : list) {
		    // If String is not in set, add it to the set.
		    if (!set.contains(item.trim())) {
		    	set.add(item.trim());
		    }
		}
		return set;
	}

	/**
	 * 
	 * @return Gson
	 */
	public static Gson getGsonInstance(){
		 if(gson == null){
			 gson = new Gson();
		 }
		 return gson;
	}

	/**
	 * 
	 * @return RestTemplate
	 */
	public static RestTemplate getRestTemplateInstance(){
		  if(restTemplate == null){
			  restTemplate = new RestTemplate();
		  }
		  return restTemplate;
	}
	/**
	 * Calculate the distance between two co-ordinates on earth in miles/km
	 * 
	 * @param srcLatitude
	 * @param srcLongitude
	 * @param destLatitude
	 * @param destLongitude
	 * @param distType
	 * @return
	 */
	
	public static double getLocationDistance(double srcLatitude, double srcLongitude, 
			double destLatitude, double destLongitude, 
			DistanceType distType) {
	    double dLat = Math.toRadians(destLatitude - srcLatitude);
	    double dLon = Math.toRadians(destLongitude - srcLongitude);
	    double a = Math.sin(dLat / 2) * Math.sin(dLat / 2) +
			       Math.cos(Math.toRadians(srcLatitude)) * Math.cos(Math.toRadians(destLatitude)) 
			      * Math.sin(dLon / 2) * Math.sin(dLon / 2);
	    double angle = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
	    double distanceInDistType = (distType.equals(DistanceType.KiloMeters) ? LithoserUtil.EARTH_RADIUS_IN_KM : LithoserUtil.EARTH_RADIUS_IN_MILES)
	    					  * angle;
	    return distanceInDistType;
	}
	
	/**
	 * 
	 * @param a
	 * @return
	 */
	public static double getAverage(double... a ) {
		double sum = 0;
		for(double i : a) {
			sum+=i;
		}
		if(a.length > 0) {
			return sum/a.length;
		}else {
			return sum;
		}
	}
	
	/**
	 * 
	 * @param type
	 * @return
	 */
	public static YesNoType getYesNoType(String type) {
		return type != null && YesNoType.Yes.getValue().equalsIgnoreCase(type)
				? YesNoType.Yes : YesNoType.No;
	}
	/**
	 * 
	 * @param type
	 * @return
	 */
	public static SexType getSexType(String type) {
		if(SexType.Male.getValue().equalsIgnoreCase(type)) {
			return SexType.Male;
		}else if(SexType.Female.getValue().equalsIgnoreCase(type)) {
			return SexType.Female;			
		}else {
			return SexType.Other;			
		}		
	}
	/**
	 * 
	 * @param type
	 * @return
	 */
	public static ProfileType getProfileType(String type) {
		if(ProfileType.Vendor.getValue().equalsIgnoreCase(type)) {
			return ProfileType.Vendor;
		}else if(ProfileType.Technician.getValue().equalsIgnoreCase(type)) {
			return ProfileType.Technician;			
		}else {
			return ProfileType.Customer;		
		}		
	}
	/**
	 * 
	 * @param value
	 * @return
	 */
	public static Number convertToNumber(Number value, Class<?> clazz) {
		if(value == null) {
			if(Double.class.getName().compareToIgnoreCase(clazz.getName()) == 0) {
				value = new Double(0);
			} else if(Long.class.getName().compareToIgnoreCase(clazz.getName()) == 0) {
				value = new Long(0);
			} else if(Integer.class.getName().compareToIgnoreCase(clazz.getName()) == 0) {
				value = new Integer(0);
			}
		}
		return value;
	}
}