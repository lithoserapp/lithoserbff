package com.lithoser.common.exceptions;

/**
 * 
 * @author Sanjay
 *
 */
@SuppressWarnings("serial")
public class LithoserException extends Exception{
	public LithoserException() {}
	
	public LithoserException(String message)
    {
       super(message);
    }
}
