package com.lithoser.common.controller;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.lithoser.common.dto.GeneralResponseDto;
import com.lithoser.common.dto.ProfileDetailsContainerDto;
import com.lithoser.common.dto.ProfileDetailsDto;
import com.lithoser.common.service.LithoserService;
import com.lithoser.common.service.LithoserServiceImpl;
import com.lithoser.common.util.LithoserUtil;
import com.lithoser.common.util.LogUtil;
import com.lithoser.common.util.ResponseUtil;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;

/**
 * 
 * @author Sanjay
 *
 */

@RestController
public class LithoserProfileController {

	// Example http://localhost:8080/lithoser/profile/create/
	@RequestMapping(value = "/profile/create", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public ResponseEntity<GeneralResponseDto> registerAProfile(
			@RequestBody ProfileDetailsDto requestProfileDetailsDto) {
		LogUtil.LOG.info("Lithoser registerAProfile, Request :\n"
				+ LithoserUtil.getGsonInstance().toJson(requestProfileDetailsDto));
		try {
			LithoserService lithoserService = new LithoserServiceImpl();
			if (!lithoserService.registerAProfile(requestProfileDetailsDto)) {
				return ResponseUtil.createBadRequestError("Parameter missing or bad request object");
			}
		} catch (Exception ex) {
			LogUtil.LOG.warn(ex.getMessage());
			ex.printStackTrace();
			LogUtil.LOG.warn("Exception BAD_REQUEST - " + HttpStatus.BAD_REQUEST.value());
			return ResponseUtil
					.createBadRequestError("Exception BAD_REQUEST : Parameter missing or bad request object");
		}
		return ResponseUtil.createSuccess("Profile created successfully");
	}

	// Example http://localhost:8080/lithoser/profile/update
	@RequestMapping(value = "/profile/update", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public ResponseEntity<GeneralResponseDto> updateAProfile(@RequestBody ProfileDetailsDto requestProfileDetailsDto) {
		LogUtil.LOG.info("Lithoser updateAProfile, Request :\n"
				+ LithoserUtil.getGsonInstance().toJson(requestProfileDetailsDto));
		try {
			LithoserService lithoserService = new LithoserServiceImpl();
			if (!lithoserService.updateAProfile(requestProfileDetailsDto)) {
				return ResponseUtil.createBadRequestError("Parameter missing or bad request object");
			}
		} catch (Exception ex) {
			LogUtil.LOG.warn(ex.getMessage());
			ex.printStackTrace();
			LogUtil.LOG.warn("Exception BAD_REQUEST - " + HttpStatus.BAD_REQUEST.value());
			return ResponseUtil
					.createBadRequestError("Exception BAD_REQUEST : Parameter missing or bad request object");
		}
		return ResponseUtil.createSuccess("Profile updated successfully");
	}
	// TODO API to provide list of profiles

	// Example
	// http://localhost:8080/lithoser/profile/detailsByPhoneNumber/5137778989/T
	@RequestMapping(value = "/profile/detailsByPhoneNumber/{phoneNumber}/{userType}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public ResponseEntity<ProfileDetailsContainerDto> getProfileDetails(@PathVariable String phoneNumber,
			@PathVariable String userType) {
		ProfileDetailsContainerDto profileDetailsContainerDto = new ProfileDetailsContainerDto();
		try {
			LogUtil.LOG.info("Lithoser getProfileDetails, Request :\n" + phoneNumber);
			LithoserService lithoserService = new LithoserServiceImpl();
			profileDetailsContainerDto
					.setProfileDetails(lithoserService.getProfileDetailsByPhoneNumber(phoneNumber, userType));
			profileDetailsContainerDto.setMessage(ResponseUtil.SUCCESS);
			profileDetailsContainerDto.setStatus(ResponseUtil.SUCCESS);
		} catch (Exception ex) {
			LogUtil.LOG.warn(ex.getMessage());
			ex.printStackTrace();
			LogUtil.LOG.warn("Exception BAD_REQUEST - " + HttpStatus.BAD_REQUEST.value());
			profileDetailsContainerDto.setMessage(ResponseUtil.BAD_REQUEST);
			profileDetailsContainerDto.setStatus(ResponseUtil.FAILURE_STATUS);
			return new ResponseEntity<ProfileDetailsContainerDto>(profileDetailsContainerDto, HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity<ProfileDetailsContainerDto>(profileDetailsContainerDto, HttpStatus.OK);
	}

	// Example
	// http://localhost:8080/lithoser/profile/detailsByProfileId/326
	@RequestMapping(value = "/profile/detailsByProfileId/{profileId}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public ResponseEntity<ProfileDetailsContainerDto> getProfileDetails(@PathVariable Long profileId) {
		ProfileDetailsContainerDto profileDetailsContainerDto = new ProfileDetailsContainerDto();
		try {
			LogUtil.LOG.info("Lithoser getProfileDetails, Request :\n" + profileId);
			LithoserService lithoserService = new LithoserServiceImpl();
			profileDetailsContainerDto
					.setProfileDetails(lithoserService.getProfileDetailsByProfileId(profileId));
			profileDetailsContainerDto.setMessage(ResponseUtil.SUCCESS);
			profileDetailsContainerDto.setStatus(ResponseUtil.SUCCESS);
		} catch (Exception ex) {
			LogUtil.LOG.warn(ex.getMessage());
			ex.printStackTrace();
			LogUtil.LOG.warn("Exception BAD_REQUEST - " + HttpStatus.BAD_REQUEST.value());
			profileDetailsContainerDto.setMessage(ResponseUtil.BAD_REQUEST);
			profileDetailsContainerDto.setStatus(ResponseUtil.FAILURE_STATUS);
			return new ResponseEntity<ProfileDetailsContainerDto>(profileDetailsContainerDto, HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity<ProfileDetailsContainerDto>(profileDetailsContainerDto, HttpStatus.OK);
	}

	// Example http://localhost:8080/lithoser/serverStatus
	@RequestMapping(value = "/serverStatus", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public ResponseEntity<GeneralResponseDto> getServerStatus() {
		GeneralResponseDto data = new GeneralResponseDto();
		try {
			data.setMessage("Alive!!!");
			data.setStatus("Success");
		} catch (Exception ex) {
			return new ResponseEntity<GeneralResponseDto>(data, HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity<GeneralResponseDto>(data, HttpStatus.OK);
	}
}