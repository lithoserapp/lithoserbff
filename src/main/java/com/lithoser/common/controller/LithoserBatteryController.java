package com.lithoser.common.controller;

import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.lithoser.common.dto.BatterySaleDetailsContainerDto;
import com.lithoser.common.dto.BatterySaleDetailsDto;
import com.lithoser.common.dto.BatterySaleDetailsListDto;
import com.lithoser.common.dto.GeneralResponseDto;
import com.lithoser.common.dto.ProfileDetailsContainerDto;
import com.lithoser.common.dto.ProfileDetailsDto;
import com.lithoser.common.service.LithoserService;
import com.lithoser.common.service.LithoserServiceImpl;
import com.lithoser.common.util.LithoserUtil;
import com.lithoser.common.util.LogUtil;
import com.lithoser.common.util.ProfileType;
import com.lithoser.common.util.ResponseUtil;

import java.util.List;
import java.util.Map;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;

/**
 * 
 * @author Sanjay
 *
 */

@RestController
public class LithoserBatteryController {
	
	// Example http://localhost:8080/lithoser/battery/createSale
	@RequestMapping(value = "/battery/createSale", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public ResponseEntity<GeneralResponseDto> registerABatterySale(@RequestBody BatterySaleDetailsDto requestBatterySellDetailsDto) {
		LogUtil.LOG.info(
				"Lithoser registerABatterySale, Request :\n" + LithoserUtil.getGsonInstance().toJson(requestBatterySellDetailsDto));
		try {
			LithoserService lithoserService = new LithoserServiceImpl();
			if(requestBatterySellDetailsDto.getSoldToProfileId() == null &&
					requestBatterySellDetailsDto.getCustomerPhoneNumber() != null) {
				ProfileDetailsDto profileDetailsDto = new ProfileDetailsDto();
				profileDetailsDto = lithoserService.getProfileDetailsByPhoneNumber(requestBatterySellDetailsDto.getCustomerPhoneNumber(), ProfileType.Customer.getValue());
				requestBatterySellDetailsDto.setSoldToProfileId(profileDetailsDto.getProfileId());
			}else if(requestBatterySellDetailsDto.getSoldToProfileId() == null &&
					requestBatterySellDetailsDto.getCustomerPhoneNumber() == null) {
				LogUtil.LOG.warn("Exception BAD_REQUEST - " + HttpStatus.BAD_REQUEST.value());
				return ResponseUtil.createBadRequestError("Exception registerABatterySale BAD_REQUEST : SoldToPhoneNumber Parameter missing or bad request object");
			}
			if(!lithoserService.registerABatterySale(requestBatterySellDetailsDto)) {
				return ResponseUtil.createBadRequestError("registerABatterySale: Parameter missing or bad request object");
			}			
		} catch (Exception ex) {
			LogUtil.LOG.warn(ex.getMessage());
			ex.printStackTrace();
			LogUtil.LOG.warn("Exception BAD_REQUEST - " + HttpStatus.BAD_REQUEST.value());
			return ResponseUtil.createBadRequestError("Exception registerABatterySale BAD_REQUEST : Parameter missing or bad request object");
		}
		return ResponseUtil.createSuccess("Battery sale created successfully");
	}
	
	// Example http://localhost:8080/lithoser/battery/updateSale/
	@RequestMapping(value = "/battery/updateSale/", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public ResponseEntity<GeneralResponseDto> updateABatterySale(@RequestBody BatterySaleDetailsDto requestBatterySaleDetailsDto) {
		LogUtil.LOG.info(
				"Lithoser updateABatterySale, Request :\n" + LithoserUtil.getGsonInstance().toJson(requestBatterySaleDetailsDto));
		try {
			LithoserService lithoserService = new LithoserServiceImpl();
			if(!lithoserService.updateABatterySale(requestBatterySaleDetailsDto)) {
				return ResponseUtil.createBadRequestError("updateABatterySale: Parameter missing or bad request object");
			}			
		} catch (Exception ex) {
			LogUtil.LOG.warn(ex.getMessage());
			ex.printStackTrace();
			LogUtil.LOG.warn("Exception BAD_REQUEST - " + HttpStatus.BAD_REQUEST.value());
			return ResponseUtil.createBadRequestError("Exception updateABatterySale BAD_REQUEST : Parameter missing or bad request object");
		}
		return ResponseUtil.createSuccess("Battery sale updated successfully");
	}
	
	// Example http://localhost:8080/lithoser/battery/updateActiveStatusOfSale?activeStatus=N&saleDetailsId=1241415
	@RequestMapping(value = "/battery/updateActiveStatusOfSale", params= {"activeStatus","saleDetailsId"}, method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public ResponseEntity<GeneralResponseDto> updateActiveStatusOfSale(@RequestParam String activeStatus, @RequestParam Long saleDetailsId) {
		LogUtil.LOG.info(
				"Lithoser updateActiveStatusOfSale, ActiveStatus :\n" + activeStatus + " for saleDetailsId :" + saleDetailsId);
		try {
			LithoserService lithoserService = new LithoserServiceImpl();
			if(!lithoserService.updateActiveStatusOfBatterySale(activeStatus, saleDetailsId)) {
				return ResponseUtil.createBadRequestError("updateActiveStatusOfSale: Parameter missing or bad request object");
			}			
		} catch (Exception ex) {
			LogUtil.LOG.warn(ex.getMessage());
			ex.printStackTrace();
			LogUtil.LOG.warn("Exception BAD_REQUEST - " + HttpStatus.BAD_REQUEST.value());
			return ResponseUtil.createBadRequestError("Exception updateABatterySale BAD_REQUEST : Parameter missing or bad request object");
		}
		return ResponseUtil.createSuccess("Battery sale active status updated successfully to "+ activeStatus);
	}
	// Example http://localhost:8080/lithoser/battery/saleDetails?saleDetailsId=1241415
	@RequestMapping(value = "/battery/saleDetails", params= {"saleDetailsId"}, method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public ResponseEntity<BatterySaleDetailsContainerDto> getABatterySaleDetails(@RequestParam Long saleDetailsId) {
		 BatterySaleDetailsContainerDto batterySaleDetailsContainer = new BatterySaleDetailsContainerDto();
		try {
			LogUtil.LOG.info(
					"Lithoser getABatterySaleDetails, Request :\n" + saleDetailsId);
			LithoserService lithoserService = new LithoserServiceImpl();
			BatterySaleDetailsDto batterySaleDetails = lithoserService.getABatterySaleDetails(saleDetailsId);
			batterySaleDetailsContainer.setBatterySaleDetailsDto(batterySaleDetails);
			batterySaleDetailsContainer.setMessage(ResponseUtil.SUCCESS);
			batterySaleDetailsContainer.setStatus(ResponseUtil.SUCCESS);
		} catch (Exception ex) {
			LogUtil.LOG.warn(ex.getMessage());
			ex.printStackTrace();
			LogUtil.LOG.warn("Exception BAD_REQUEST - " + HttpStatus.BAD_REQUEST.value());
			batterySaleDetailsContainer.setMessage(ResponseUtil.BAD_REQUEST);
			batterySaleDetailsContainer.setStatus(ResponseUtil.FAILURE_STATUS);
			return new ResponseEntity<BatterySaleDetailsContainerDto>(batterySaleDetailsContainer, HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity<BatterySaleDetailsContainerDto>(batterySaleDetailsContainer, HttpStatus.OK);
	}	
	
	// Example http://localhost:8080/lithoser/battery/saleDetailsListBySeller?sellerProfileId=1241415&machineBatteryName=duralcell
	@RequestMapping(value = "/battery/saleDetailsListBySeller", params= {"sellerProfileId","machineBatteryName"}, method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public ResponseEntity<BatterySaleDetailsListDto> getSaleDetailsListBySeller(
			@RequestParam Long sellerProfileId,
			@RequestParam String machineBatteryName) {
		BatterySaleDetailsListDto batterySaleDetailsListDto = new BatterySaleDetailsListDto();
		try {
			LogUtil.LOG.info(
					"Lithoser getSaleDetailsListBySeller, Request :\n" + sellerProfileId);
			LithoserService lithoserService = new LithoserServiceImpl();
			List<BatterySaleDetailsDto> batterySaleDetailsList = lithoserService.getListOfBatterySaleDetailsBySeller(sellerProfileId, machineBatteryName);
			batterySaleDetailsListDto.setBatterySaleDetailsList(batterySaleDetailsList);
			batterySaleDetailsListDto.setMessage(ResponseUtil.SUCCESS);
			batterySaleDetailsListDto.setStatus(ResponseUtil.SUCCESS);
		} catch (Exception ex) {
			LogUtil.LOG.warn(ex.getMessage());
			ex.printStackTrace();
			LogUtil.LOG.warn("getSaleDetailsListBySeller: Exception BAD_REQUEST - " + HttpStatus.BAD_REQUEST.value());
			batterySaleDetailsListDto.setMessage(ResponseUtil.BAD_REQUEST);
			batterySaleDetailsListDto.setStatus(ResponseUtil.FAILURE_STATUS);
			return new ResponseEntity<BatterySaleDetailsListDto>(batterySaleDetailsListDto, HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity<BatterySaleDetailsListDto>(batterySaleDetailsListDto, HttpStatus.OK);
	}	
	// Example http://localhost:8080/lithoser/battery/saleDetailsListBySellerAndBuyer?sellerProfileId=1241415&buyerProfileId=342345
	@RequestMapping(value = "/battery/saleDetailsListBySellerAndBuyer", params= {"sellerProfileId","buyerProfileId"}, method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public ResponseEntity<BatterySaleDetailsListDto> getSaleDetailsListBySellerAndBuyer(
			@RequestParam Long sellerProfileId,
			@RequestParam Long buyerProfileId) {
		BatterySaleDetailsListDto batterySaleDetailsListDto = new BatterySaleDetailsListDto();
		try {
			LogUtil.LOG.info(
					"Lithoser saleDetailsListBySellerAndBuyer, Request :\n sellerProfiledId" + sellerProfileId+ " buyerId:"+buyerProfileId);
			LithoserService lithoserService = new LithoserServiceImpl();
			List<BatterySaleDetailsDto> batterySaleDetailsList = 
					lithoserService.getListOfBatterySaleDetailsBySellerAndBuyer(
							sellerProfileId, 
							buyerProfileId);
			batterySaleDetailsListDto.setBatterySaleDetailsList(batterySaleDetailsList);
			batterySaleDetailsListDto.setMessage(ResponseUtil.SUCCESS);
			batterySaleDetailsListDto.setStatus(ResponseUtil.SUCCESS);
		} catch (Exception ex) {
			LogUtil.LOG.warn(ex.getMessage());
			ex.printStackTrace();
			LogUtil.LOG.warn("getSaleDetailsListBySellerAndBuyer: Exception BAD_REQUEST - " + HttpStatus.BAD_REQUEST.value());
			batterySaleDetailsListDto.setMessage(ResponseUtil.BAD_REQUEST);
			batterySaleDetailsListDto.setStatus(ResponseUtil.FAILURE_STATUS);
			return new ResponseEntity<BatterySaleDetailsListDto>(batterySaleDetailsListDto, HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity<BatterySaleDetailsListDto>(batterySaleDetailsListDto, HttpStatus.OK);
	}	
}