package com.lithoser.common.dto;

import java.io.Serializable;

/**
 * 
 * @author Sanjay
 *
 */
public class ProfileDetailsContainerDto implements Serializable{

	private static final long serialVersionUID = -1;

	private String status;
	private String message;
	private ProfileDetailsDto profileDetails;
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public ProfileDetailsDto getProfileDetails() {
		return profileDetails;
	}
	public void setProfileDetails(ProfileDetailsDto profileDetails) {
		this.profileDetails = profileDetails;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((message == null) ? 0 : message.hashCode());
		result = prime * result + ((profileDetails == null) ? 0 : profileDetails.hashCode());
		result = prime * result + ((status == null) ? 0 : status.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ProfileDetailsContainerDto other = (ProfileDetailsContainerDto) obj;
		if (message == null) {
			if (other.message != null)
				return false;
		} else if (!message.equals(other.message))
			return false;
		if (profileDetails == null) {
			if (other.profileDetails != null)
				return false;
		} else if (!profileDetails.equals(other.profileDetails))
			return false;
		if (status == null) {
			if (other.status != null)
				return false;
		} else if (!status.equals(other.status))
			return false;
		return true;
	}
	
	
}
