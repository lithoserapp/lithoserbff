package com.lithoser.common.dto;

import java.io.Serializable;
import java.util.List;

/**
 * 
 * @author Sanjay
 *
 */
public class ProfileDetailsListDto implements Serializable{

	private static final long serialVersionUID = -1;

	private String status;
	private String message;
	private List<ProfileDetailsDto> profileDetailsList;
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public List<ProfileDetailsDto> getProfileDetailsList() {
		return profileDetailsList;
	}
	public void setProfileDetailsList(List<ProfileDetailsDto> profileDetailsList) {
		this.profileDetailsList = profileDetailsList;
	}	
}
