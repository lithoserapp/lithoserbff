package com.lithoser.common.dto;

import java.io.Serializable;
import java.util.List;

/**
 * 
 * @author Sanjay
 *
 */
public class BatterySaleDetailsListDto implements Serializable{

	private static final long serialVersionUID = -1;

	private String status;
	private String message;
	private List<BatterySaleDetailsDto> batterySaleDetailsList;
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public List<BatterySaleDetailsDto> getBatterySaleDetailsList() {
		return batterySaleDetailsList;
	}
	public void setBatterySaleDetailsList(List<BatterySaleDetailsDto> batterySaleDetailsList) {
		this.batterySaleDetailsList = batterySaleDetailsList;
	}
	
	
}
