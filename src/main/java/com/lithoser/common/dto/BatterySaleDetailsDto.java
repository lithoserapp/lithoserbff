package com.lithoser.common.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;

/**
 * 
 * @author Sanjay
 *
 */
public class BatterySaleDetailsDto implements Serializable{

	private static final long serialVersionUID = -1;

	private Long saleDetailsId;
	private String batteryName;
	private Timestamp batteryManufacturingDate;
	private String batteryImage;
	private Long soldToProfileId;
	private String customerPhoneNumber;
	private Long soldByProfileId;
	private Integer notificationConfiguration;
	private BigDecimal salesPrice;
	private BigDecimal customerPaidAmt;
	private BigDecimal discountAmt;
	private String salesComment;
	private Timestamp saleDate;
	private Timestamp installedDate;
	private Integer lifeExpectancyInDays;
	private Integer quantity;
	private String batteryModel;
	
	public Integer getQuantity() {
		return quantity;
	}
	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}
	public Long getSaleDetailsId() {
		return saleDetailsId;
	}
	public void setSaleDetailsId(Long saleDetailsId) {
		this.saleDetailsId = saleDetailsId;
	}
	public String getBatteryName() {
		return batteryName;
	}
	public void setBatteryName(String batteryName) {
		this.batteryName = batteryName;
	}
	public Timestamp getBatteryManufacturingDate() {
		return batteryManufacturingDate;
	}
	public void setBatteryManufacturingDate(Timestamp batteryManufacturingDate) {
		this.batteryManufacturingDate = batteryManufacturingDate;
	}
	public String getBatteryImage() {
		return batteryImage;
	}
	public void setBatteryImage(String batteryImage) {
		this.batteryImage = batteryImage;
	}
	public Long getSoldToProfileId() {
		return soldToProfileId;
	}
	public void setSoldToProfileId(Long soldToProfileId) {
		this.soldToProfileId = soldToProfileId;
	}
	public Long getSoldByProfileId() {
		return soldByProfileId;
	}
	public void setSoldByProfileId(Long soldByProfileId) {
		this.soldByProfileId = soldByProfileId;
	}
	public Integer getNotificationConfiguration() {
		return notificationConfiguration;
	}
	public void setNotificationConfiguration(Integer notificationConfiguration) {
		this.notificationConfiguration = notificationConfiguration;
	}
	public BigDecimal getSalesPrice() {
		return salesPrice;
	}
	public void setSalesPrice(BigDecimal salesPrice) {
		this.salesPrice = salesPrice;
	}
	public BigDecimal getCustomerPaidAmt() {
		return customerPaidAmt;
	}
	public void setCustomerPaidAmt(BigDecimal customerPaidAmt) {
		this.customerPaidAmt = customerPaidAmt;
	}
	public BigDecimal getDiscountAmt() {
		return discountAmt;
	}
	public void setDiscountAmt(BigDecimal discountAmt) {
		this.discountAmt = discountAmt;
	}
	public String getSalesComment() {
		return salesComment;
	}
	public void setSalesComment(String salesComment) {
		this.salesComment = salesComment;
	}
	public Timestamp getSaleDate() {
		return saleDate;
	}
	public void setSaleDate(Timestamp saleDate) {
		this.saleDate = saleDate;
	}
	public Timestamp getInstalledDate() {
		return installedDate;
	}
	public void setInstalledDate(Timestamp installedDate) {
		this.installedDate = installedDate;
	}
	public Integer getLifeExpectancyInDays() {
		return lifeExpectancyInDays;
	}
	public void setLifeExpectancyInDays(Integer lifeExpectancyInDays) {
		this.lifeExpectancyInDays = lifeExpectancyInDays;
	}
	public String getBatteryModel() {
		return batteryModel;
	}
	public void setBatteryModel(String batteryModel) {
		this.batteryModel = batteryModel;
	}
	public String getCustomerPhoneNumber() {
		return customerPhoneNumber;
	}
	public void setCustomerPhoneNumber(String customerPhoneNumber) {
		this.customerPhoneNumber = customerPhoneNumber;
	}
	
}
