package com.lithoser.common.dto;

import java.io.Serializable;
import java.sql.Timestamp;

import com.lithoser.common.util.LithoserUtil;
import com.lithoser.common.util.ProfileType;
import com.lithoser.common.util.SexType;
import com.lithoser.common.util.YesNoType;

/**
 * 
 * @author Sanjay
 *
 */
public class ProfileDetailsDto implements Serializable{

	private static final long serialVersionUID = -1;

	private Long profileId;
	private String firstName;
	private String middleName;
	private String lastName;
	private String emailId;
	private String phoneNumber;
	private String dateOfBirth;
	private SexType sex;
	private String addressStreetName;
	private String cityZipcode;
	private ProfileType profileType;
	private YesNoType isPushNotificationEnabled;
	private YesNoType isEmailNotificationEnabled;
	private YesNoType activeStatus;
	private Timestamp profileUpdateDate;
	
	public Long getProfileId() {
		return profileId;
	}
	public void setProfileId(Long profileId) {
		this.profileId = profileId;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getMiddleName() {
		return middleName;
	}
	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getEmailId() {
		return emailId;
	}
	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}
	public String getPhoneNumber() {
		return LithoserUtil.getContactNumber(phoneNumber);
	}
	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = LithoserUtil.getContactNumber(phoneNumber);
	}
	public String getDateOfBirth() {
		return dateOfBirth;
	}
	public void setDateOfBirth(String dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}
	public SexType getSex() {
		return sex;
	}
	public void setSex(SexType sex) {
		this.sex = sex;
	}
	public String getAddressStreetName() {
		return addressStreetName;
	}
	public void setAddressStreetName(String addressStreetName) {
		this.addressStreetName = addressStreetName;
	}
	public String getCityZipcode() {
		return cityZipcode;
	}
	public void setCityZipcode(String cityZipcode) {
		this.cityZipcode = cityZipcode;
	}
	public YesNoType getIsPushNotificationEnabled() {
		return isPushNotificationEnabled;
	}
	public void setIsPushNotificationEnabled(YesNoType isPushNotificationEnabled) {
		this.isPushNotificationEnabled = isPushNotificationEnabled;
	}
	public YesNoType getIsEmailNotificationEnabled() {
		return isEmailNotificationEnabled;
	}
	public void setIsEmailNotificationEnabled(YesNoType isEmailNotificationEnabled) {
		this.isEmailNotificationEnabled = isEmailNotificationEnabled;
	}
	public YesNoType getActiveStatus() {
		return activeStatus;
	}
	public void setActiveStatus(YesNoType activeStatus) {
		this.activeStatus = activeStatus;
	}
	public Timestamp getProfileUpdateDate() {
		return profileUpdateDate;
	}
	public void setProfileUpdateDate(Timestamp profileUpdateDate) {
		this.profileUpdateDate = profileUpdateDate;
	}
	public ProfileType getProfileType() {
		return profileType;
	}
	public void setProfileType(ProfileType profileType) {
		this.profileType = profileType;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((activeStatus == null) ? 0 : activeStatus.hashCode());
		result = prime * result + ((addressStreetName == null) ? 0 : addressStreetName.hashCode());
		result = prime * result + ((cityZipcode == null) ? 0 : cityZipcode.hashCode());
		result = prime * result + ((dateOfBirth == null) ? 0 : dateOfBirth.hashCode());
		result = prime * result + ((emailId == null) ? 0 : emailId.hashCode());
		result = prime * result + ((firstName == null) ? 0 : firstName.hashCode());
		result = prime * result + ((isEmailNotificationEnabled == null) ? 0 : isEmailNotificationEnabled.hashCode());
		result = prime * result + ((isPushNotificationEnabled == null) ? 0 : isPushNotificationEnabled.hashCode());
		result = prime * result + ((lastName == null) ? 0 : lastName.hashCode());
		result = prime * result + ((middleName == null) ? 0 : middleName.hashCode());
		result = prime * result + ((phoneNumber == null) ? 0 : phoneNumber.hashCode());
		result = prime * result + ((profileId == null) ? 0 : profileId.hashCode());
		result = prime * result + ((profileType == null) ? 0 : profileType.hashCode());
		result = prime * result + ((profileUpdateDate == null) ? 0 : profileUpdateDate.hashCode());
		result = prime * result + ((sex == null) ? 0 : sex.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ProfileDetailsDto other = (ProfileDetailsDto) obj;
		if (activeStatus != other.activeStatus)
			return false;
		if (addressStreetName == null) {
			if (other.addressStreetName != null)
				return false;
		} else if (!addressStreetName.equals(other.addressStreetName))
			return false;
		if (cityZipcode == null) {
			if (other.cityZipcode != null)
				return false;
		} else if (!cityZipcode.equals(other.cityZipcode))
			return false;
		if (dateOfBirth == null) {
			if (other.dateOfBirth != null)
				return false;
		} else if (!dateOfBirth.equals(other.dateOfBirth))
			return false;
		if (emailId == null) {
			if (other.emailId != null)
				return false;
		} else if (!emailId.equals(other.emailId))
			return false;
		if (firstName == null) {
			if (other.firstName != null)
				return false;
		} else if (!firstName.equals(other.firstName))
			return false;
		if (isEmailNotificationEnabled != other.isEmailNotificationEnabled)
			return false;
		if (isPushNotificationEnabled != other.isPushNotificationEnabled)
			return false;
		if (lastName == null) {
			if (other.lastName != null)
				return false;
		} else if (!lastName.equals(other.lastName))
			return false;
		if (middleName == null) {
			if (other.middleName != null)
				return false;
		} else if (!middleName.equals(other.middleName))
			return false;
		if (phoneNumber == null) {
			if (other.phoneNumber != null)
				return false;
		} else if (!phoneNumber.equals(other.phoneNumber))
			return false;
		if (profileId == null) {
			if (other.profileId != null)
				return false;
		} else if (!profileId.equals(other.profileId))
			return false;
		if (profileType != other.profileType)
			return false;
		if (profileUpdateDate == null) {
			if (other.profileUpdateDate != null)
				return false;
		} else if (!profileUpdateDate.equals(other.profileUpdateDate))
			return false;
		if (sex != other.sex)
			return false;
		return true;
	}
	
}
