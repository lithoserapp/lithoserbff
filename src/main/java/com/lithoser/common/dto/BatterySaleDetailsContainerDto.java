package com.lithoser.common.dto;

import java.io.Serializable;

/**
 * 
 * @author Sanjay
 *
 */
public class BatterySaleDetailsContainerDto implements Serializable{

	private static final long serialVersionUID = -1;

	private String status;
	private String message;
	private BatterySaleDetailsDto batterySaleDetailsDto;
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public BatterySaleDetailsDto getBatterySaleDetailsDto() {
		return batterySaleDetailsDto;
	}
	public void setBatterySaleDetailsDto(BatterySaleDetailsDto batterySaleDetailsDto) {
		this.batterySaleDetailsDto = batterySaleDetailsDto;
	}
	
}
