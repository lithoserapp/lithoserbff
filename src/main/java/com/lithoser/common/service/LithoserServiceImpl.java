package com.lithoser.common.service;

import java.sql.SQLException;
import java.util.List;

import com.lithoser.common.dto.BatterySaleDetailsDto;
import com.lithoser.common.dto.ProfileDetailsDto;
import com.lithoser.common.pojo.CityInfo;
import com.lithoser.common.util.LithoserUtil;

/**
 * 
 * @author Sanjay
 *
 */
public class LithoserServiceImpl implements LithoserService {

	/**
	 * fetch the profile details by phone number and userType
	 * 
	 * @param phoneNumber String
	 * @return userType<String>
	 */
	@Override
	public ProfileDetailsDto getProfileDetailsByPhoneNumber(String phoneNumber, String userType) throws SQLException {
		ProfileDetailsDto profileDetailsDto = new ProfileDetailsDto();
		profileDetailsDto = lithoserDAO.getProfileDetailsByPhoneNumber(phoneNumber, userType);
		if(profileDetailsDto.getPhoneNumber() == null || 
				(profileDetailsDto.getPhoneNumber() != null && profileDetailsDto.getPhoneNumber().isEmpty() )) {
			profileDetailsDto.setPhoneNumber(phoneNumber);
			profileDetailsDto.setSex(LithoserUtil.getSexType("O"));
			profileDetailsDto.setIsEmailNotificationEnabled(LithoserUtil.getYesNoType("Y"));
			profileDetailsDto.setIsPushNotificationEnabled(LithoserUtil.getYesNoType("Y"));
			profileDetailsDto.setProfileType(LithoserUtil.getProfileType(userType));
			profileDetailsDto.setActiveStatus(LithoserUtil.getYesNoType("Y"));
			if(registerAProfile(profileDetailsDto)) {
				profileDetailsDto = lithoserDAO.getProfileDetailsByPhoneNumber(phoneNumber, userType);
			}
		}
		return profileDetailsDto;
	}
	
	/**
	  * fetch the profile details by profileId
	 * 
	 * @param profileId Long
	 */
	@Override
	public ProfileDetailsDto getProfileDetailsByProfileId(Long profileId) throws SQLException {
		ProfileDetailsDto profileDetailsDto = new ProfileDetailsDto();
		profileDetailsDto = lithoserDAO.getProfileDetailsByProfileId(profileId);
		return profileDetailsDto;
	}
	
	@Override
	public Boolean registerAProfile(ProfileDetailsDto profileDetailsDto) throws SQLException {
		return lithoserDAO.registerAProfile(profileDetailsDto);
	}


	@Override
	public Boolean updateAProfile(ProfileDetailsDto profileDetailsDto) throws SQLException {
		return lithoserDAO.updateAProfile(profileDetailsDto);
	}


	@Override
	public Boolean registerABatterySale(BatterySaleDetailsDto batterySellDetailsDto) throws SQLException {
		return lithoserDAO.registerABatterySale(batterySellDetailsDto);
	}
	
	@Override
	public Boolean updateABatterySale(BatterySaleDetailsDto batterySellDetailsDto) throws SQLException {
		return lithoserDAO.updateABatterySale(batterySellDetailsDto);
	}
	
	@Override
	public Boolean updateActiveStatusOfBatterySale(String activeStatus, Long saleDetailId) throws SQLException{
		return lithoserDAO.updateActiveStatusOfBatterySale(activeStatus, saleDetailId);
	}
	@Override
	public BatterySaleDetailsDto getABatterySaleDetails(Long saleDetailsId) throws SQLException {
		return lithoserDAO.getABatterySaleDetailsBySaleId(saleDetailsId);
	}

	

	@Override
	public List<BatterySaleDetailsDto> getListOfBatterySaleDetailsBySeller(Long sellerProfileId, String machineBatteryName) throws SQLException {
		return lithoserDAO.getListOfBatterySaleDetailsBySeller(sellerProfileId, machineBatteryName);
	}


	@Override
	public List<BatterySaleDetailsDto> getListOfBatterySaleDetailsBySellerAndBuyer(Long sellerProfileId,
			Long buyerProfileId) throws SQLException {
		return lithoserDAO.getListOfBatterySaleDetailsBySellerAndBuyer(sellerProfileId, buyerProfileId);
	}


	/**
	 * fetch the latitude and longitude when given a city name or zipcode
	 * 
	 * @param cityName
	 * @param cityZipcode
	 * @return CityInfo
	 */
	@Override
	public CityInfo getCityInformation(String cityName, String stateCode, String cityZipcode) throws SQLException {
		CityInfo cityInfo = lithoserDAO.getCityInformation(cityName, stateCode, cityZipcode);
		return cityInfo;
	}		
}
