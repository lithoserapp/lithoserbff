package com.lithoser.common.service;

import java.sql.SQLException;
import java.util.List;

import org.springframework.context.ApplicationContext;

import com.lithoser.common.dao.LithoserDao;
import com.lithoser.common.dto.BatterySaleDetailsDto;
import com.lithoser.common.dto.ProfileDetailsDto;
import com.lithoser.common.pojo.CityInfo;
import com.lithoser.common.util.LithoserUtil;

/**
 * 
 * @author Sanjay
 *
 */
public interface LithoserService {

	ApplicationContext appContext = LithoserUtil.getApplicationContext();
	LithoserDao lithoserDAO = (LithoserDao) appContext.getBean("lithoserDAO");

	/**
	 * 
	 * @param phoneNumber
	 * @param userType 
	 * @return
	 * @throws SQLException
	 */
	ProfileDetailsDto getProfileDetailsByPhoneNumber(String phoneNumber, String userType) throws SQLException;

	/**
	 * 
	 * @param profileId
	 * @return
	 * @throws SQLException
	 */
	ProfileDetailsDto getProfileDetailsByProfileId(Long profileId) throws SQLException;
	/**
	 * 
	 * @param profileDetailsDto
	 * @return
	 * @throws SQLException
	 */
	Boolean registerAProfile(ProfileDetailsDto profileDetailsDto) throws SQLException;
	/**
	 * 
	 * @param profileDetailsDto
	 * @return
	 * @throws SQLException
	 */
	Boolean updateAProfile(ProfileDetailsDto profileDetailsDto) throws SQLException;
	
	/**
	 * 
	 * @param activeStatus
	 * @param saleDetailId
	 * @return
	 * @throws SQLException
	 */
	Boolean updateActiveStatusOfBatterySale(String activeStatus, Long saleDetailId) throws SQLException;
	/**
	 * 
	 * @param batterySellDetailsDto
	 * @return
	 * @throws SQLException
	 */
	Boolean registerABatterySale(BatterySaleDetailsDto batterySellDetailsDto) throws SQLException;
	/**
	 * 
	 * @param batterySellDetailsDto
	 * @return
	 * @throws SQLException
	 */
	Boolean updateABatterySale(BatterySaleDetailsDto batterySellDetailsDto) throws SQLException;
	
	/**
	 * 
	 * @param saleDetailsId
	 * @return
	 * @throws SQLException
	 */
	
	BatterySaleDetailsDto getABatterySaleDetails(Long saleDetailsId) throws SQLException;
	
	/**
	 * 
	 * @param sellerProfileId
	 * @return
	 * @throws SQLException
	 */
	List<BatterySaleDetailsDto> getListOfBatterySaleDetailsBySeller(Long sellerProfileId, String machineBatteryName) throws SQLException;
	
	/**
	 * 
	 * @param sellerProfileId
	 * @param buyerProfileId
	 * @return
	 * @throws SQLException
	 */
	 List<BatterySaleDetailsDto> getListOfBatterySaleDetailsBySellerAndBuyer(Long sellerProfileId, 
			 Long buyerProfileId) throws SQLException;
	  
	/**
	 * 
	 * @param cityName
	 * @param stateCode
	 * @param cityZipcode
	 * @return
	 * @throws SQLException
	 */
	CityInfo getCityInformation(String cityName, String stateCode, String cityZipcode) throws SQLException;

}