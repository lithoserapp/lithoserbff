package com.lithoser.common.dao;

import java.sql.SQLException;
import java.util.List;

import javax.sql.DataSource;

import com.lithoser.common.dto.BatterySaleDetailsDto;
import com.lithoser.common.dto.ProfileDetailsDto;
import com.lithoser.common.pojo.CityInfo;

/**
 * 
 * @author Sanjay
 *
 */
public interface LithoserDao{

	  DataSource getDataSource();
	  
	  void setDataSource(DataSource dataSource);
	  
	  ProfileDetailsDto getProfileDetailsByPhoneNumber(String phoneNumber, String userType) throws SQLException;
	  ProfileDetailsDto getProfileDetailsByProfileId(Long profileId) throws SQLException;
	  Boolean registerAProfile(ProfileDetailsDto profileDetailsDto) throws SQLException;
	  Boolean updateAProfile(ProfileDetailsDto profileDetailsDto) throws SQLException;
	  Boolean registerABatterySale(BatterySaleDetailsDto batterySellDetailsDto) throws SQLException;
	  Boolean updateABatterySale(BatterySaleDetailsDto batterySellDetailsDto) throws SQLException;
	  Boolean updateActiveStatusOfBatterySale(String activeStatus, Long saleDetailId) throws SQLException;
	  BatterySaleDetailsDto getABatterySaleDetailsBySaleId(Long batterySaleDetailsId) throws SQLException;
	  List<BatterySaleDetailsDto> getListOfBatterySaleDetailsBySeller(Long sellerProfileId, String buyerPhoneNumber) throws SQLException;
	  List<BatterySaleDetailsDto> getListOfBatterySaleDetailsBySellerAndBuyer(Long sellerProfileId, Long buyerProfileId) throws SQLException;
	  
	  CityInfo getCityInformation(String name, String stateCode, String zipcode) throws SQLException;
}