package com.lithoser.common.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import com.lithoser.common.dto.BatterySaleDetailsDto;
import com.lithoser.common.dto.ProfileDetailsDto;
import com.lithoser.common.pojo.CityInfo;
import com.lithoser.common.util.DateUtility;
import com.lithoser.common.util.LithoserUtil;

/**
 * 
 * @author Sanjay
 *
 */
public class LithoserDaoImpl implements LithoserDao {
	private DataSource dataSource;

	@Override
	public void setDataSource(DataSource dataSource) {
		this.dataSource = dataSource;
	}

	@Override
	public DataSource getDataSource() {
		return this.dataSource;
	}

	private static final String GET_PROFILE_DETAILS = 
			"SELECT\r\n" + 
				"PROFILE_ID,\r\n" + 
				"FIRST_NAME,\r\n" + 
				"MIDDLE_NAME,\r\n" + 
				"LAST_NAME,\r\n" + 
				"EMAIL_ID,\r\n" + 
				"PHONE_NUMBER,\r\n" + 
				"DATE_OF_BIRTH,\r\n" + 
				"SEX,\r\n" + 
				"ADDRESS_STREET_NAME,\r\n" + 
				"CITY_ZIPCODE,\r\n" + 
				"PROFILE_TYPE,\r\n" + 
				"IS_PUSH_NOTIFICATION_ENABLED,\r\n" + 
				"IS_EMAIL_NOTIFICATION_ENABLED,\r\n" + 
				"ACTIVE_STATUS,\r\n" + 
				"DATESTAMP\r\n" + 
			"FROM PROFILE_DETAIL\r\n";
	private static final String WHERE_CLAUSE_ON_PROFILE_DETAILS = 
			" WHERE PHONE_NUMBER = ? "
			+ " AND PROFILE_TYPE = ? "
			+ " AND ACTIVE_STATUS = 'Y' ";

	/**
	 * fetch the profile details by phoneNumber
	 * 
	 * @param phoneNumber String
	 * @return ProfileDetailsContainerDto
	 */
	@Override
	public ProfileDetailsDto getProfileDetailsByPhoneNumber(String phoneNumber, String userType) throws SQLException {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		ProfileDetailsDto profileDetailsDto = new ProfileDetailsDto();
		try {
			conn = this.dataSource.getConnection();

			ps = conn.prepareStatement(GET_PROFILE_DETAILS + WHERE_CLAUSE_ON_PROFILE_DETAILS);
			ps.setString(1, LithoserUtil.getContactNumber(phoneNumber));
			ps.setString(2, userType);
			rs = ps.executeQuery();
			if(rs.next()) {
				profileDetailsDto.setActiveStatus(LithoserUtil.getYesNoType(rs.getString("ACTIVE_STATUS")));
				profileDetailsDto.setAddressStreetName(rs.getString("ADDRESS_STREET_NAME"));
				profileDetailsDto.setCityZipcode(rs.getString("CITY_ZIPCODE"));
				profileDetailsDto.setDateOfBirth(rs.getString("DATE_OF_BIRTH"));
				profileDetailsDto.setEmailId(rs.getString("EMAIL_ID"));
				profileDetailsDto.setFirstName(rs.getString("FIRST_NAME"));
				profileDetailsDto.setMiddleName(rs.getString("MIDDLE_NAME"));
				profileDetailsDto.setLastName(rs.getString("LAST_NAME"));
				profileDetailsDto.setIsEmailNotificationEnabled(LithoserUtil.getYesNoType(rs.getString("IS_EMAIL_NOTIFICATION_ENABLED")));
				profileDetailsDto.setIsPushNotificationEnabled(LithoserUtil.getYesNoType(rs.getString("IS_PUSH_NOTIFICATION_ENABLED")));
				profileDetailsDto.setPhoneNumber(rs.getString("PHONE_NUMBER"));
				profileDetailsDto.setProfileId(rs.getLong("PROFILE_ID"));
				profileDetailsDto.setProfileUpdateDate(rs.getTimestamp("DATESTAMP"));
				profileDetailsDto.setSex(LithoserUtil.getSexType(rs.getString("SEX")));
				profileDetailsDto.setProfileType(LithoserUtil.getProfileType(rs.getString("PROFILE_TYPE")));
			
			}
		} finally {
			LithoserUtil.cleanUpSQLData(ps, rs, conn, this);
		}
		return profileDetailsDto;
	}
	
	
	private static final String WHERE_CLAUSE_ON_PROFILE_DETAILS_BY_PROFILE_ID = 
			" WHERE PROFILE_ID = ? "
			+ " AND ACTIVE_STATUS = 'Y' ";
	/**
	  * fetch the profile details by profileId
	 * 
	 * @param profileId Long
	 */
	@Override
	public ProfileDetailsDto getProfileDetailsByProfileId(Long profileId) throws SQLException {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		ProfileDetailsDto profileDetailsDto = new ProfileDetailsDto();
		try {
			conn = this.dataSource.getConnection();

			ps = conn.prepareStatement(GET_PROFILE_DETAILS + WHERE_CLAUSE_ON_PROFILE_DETAILS_BY_PROFILE_ID);
			ps.setLong(1, profileId);
			rs = ps.executeQuery();
			if(rs.next()) {
				profileDetailsDto.setActiveStatus(LithoserUtil.getYesNoType(rs.getString("ACTIVE_STATUS")));
				profileDetailsDto.setAddressStreetName(rs.getString("ADDRESS_STREET_NAME"));
				profileDetailsDto.setCityZipcode(rs.getString("CITY_ZIPCODE"));
				profileDetailsDto.setDateOfBirth(rs.getString("DATE_OF_BIRTH"));
				profileDetailsDto.setEmailId(rs.getString("EMAIL_ID"));
				profileDetailsDto.setFirstName(rs.getString("FIRST_NAME"));
				profileDetailsDto.setMiddleName(rs.getString("MIDDLE_NAME"));
				profileDetailsDto.setLastName(rs.getString("LAST_NAME"));
				profileDetailsDto.setIsEmailNotificationEnabled(LithoserUtil.getYesNoType(rs.getString("IS_EMAIL_NOTIFICATION_ENABLED")));
				profileDetailsDto.setIsPushNotificationEnabled(LithoserUtil.getYesNoType(rs.getString("IS_PUSH_NOTIFICATION_ENABLED")));
				profileDetailsDto.setPhoneNumber(rs.getString("PHONE_NUMBER"));
				profileDetailsDto.setProfileId(rs.getLong("PROFILE_ID"));
				profileDetailsDto.setProfileUpdateDate(rs.getTimestamp("DATESTAMP"));
				profileDetailsDto.setSex(LithoserUtil.getSexType(rs.getString("SEX")));
				profileDetailsDto.setProfileType(LithoserUtil.getProfileType(rs.getString("PROFILE_TYPE")));
			}
		} finally {
			LithoserUtil.cleanUpSQLData(ps, rs, conn, this);
		}
		return profileDetailsDto;
	}
	private static final String INSERT_PROFILE_DETAILS = 
			"INSERT INTO PROFILE_DETAIL\r\n" + 
			"(\r\n" + 
			"FIRST_NAME,\r\n" + 
			"MIDDLE_NAME,\r\n" + 
			"LAST_NAME,\r\n" + 
			"EMAIL_ID,\r\n" + 
			"PHONE_NUMBER,\r\n" + 
			"DATE_OF_BIRTH,\r\n" + 
			"SEX,\r\n" + 
			"ADDRESS_STREET_NAME,\r\n" + 
			"CITY_ZIPCODE,\r\n" + 
			"PROFILE_TYPE,\r\n" + 
			"IS_PUSH_NOTIFICATION_ENABLED,\r\n" + 
			"IS_EMAIL_NOTIFICATION_ENABLED,\r\n" + 
			"ACTIVE_STATUS,\r\n" + 
			"DATESTAMP)\r\n" + 
			"VALUES\r\n" + 
			"(\r\n" + 
			"?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?\r\n" + 
			")";
	@Override
	public Boolean registerAProfile(ProfileDetailsDto profileDetailsDto) throws SQLException {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		Boolean status = Boolean.valueOf(false);
		try{
			conn = this.dataSource.getConnection();
			int col = 1;
			ps = conn.prepareStatement(INSERT_PROFILE_DETAILS);
			ps.setString(col++, profileDetailsDto.getFirstName());
		    ps.setString(col++, profileDetailsDto.getMiddleName());
		    ps.setString(col++, profileDetailsDto.getLastName());
		    ps.setString(col++, profileDetailsDto.getEmailId());
		    ps.setString(col++, profileDetailsDto.getPhoneNumber());
		    ps.setString(col++, profileDetailsDto.getDateOfBirth());
		    ps.setString(col++, profileDetailsDto.getSex().getValue());
		    ps.setString(col++, profileDetailsDto.getAddressStreetName());
		    ps.setString(col++, profileDetailsDto.getCityZipcode());
		    ps.setString(col++, profileDetailsDto.getProfileType().getValue());
		    ps.setString(col++, profileDetailsDto.getIsPushNotificationEnabled().getValue());
		    ps.setString(col++, profileDetailsDto.getIsEmailNotificationEnabled().getValue());
		    ps.setString(col++, profileDetailsDto.getActiveStatus().getValue());
		    ps.setTimestamp(col++, DateUtility.getCurrentTimestamp());
		    int insertCount = ps.executeUpdate();
		    if (insertCount != 1) {
		    	throw new SQLException("Insert to PROFILE_DETAILS was not successful.");
		    }
		    status = Boolean.valueOf(true);
		} finally {
			LithoserUtil.cleanUpSQLData(ps, rs, conn, this);
		}
		return status;
	}

	private static final String UPDATE_PROFILE_DETAILS = 
			"UPDATE PROFILE_DETAIL\r\n" + 
			"SET\r\n" + 
				"FIRST_NAME = ?,\r\n" + 
				"MIDDLE_NAME = ?,\r\n" + 
				"LAST_NAME = ?,\r\n" + 
				"EMAIL_ID = ?,\r\n" + 
				"DATE_OF_BIRTH = ?,\r\n" + 
				"SEX = ?,\r\n" + 
				"ADDRESS_STREET_NAME = ?,\r\n" + 
				"CITY_ZIPCODE = ?,\r\n" + 
				"IS_PUSH_NOTIFICATION_ENABLED = ?,\r\n" + 
				"IS_EMAIL_NOTIFICATION_ENABLED = ?,\r\n" + 
				"ACTIVE_STATUS = ?,\r\n" + 
				"DATESTAMP = ?\r\n" + 
			"WHERE PHONE_NUMBER = ? "
			+ "AND PROFILE_TYPE = ? ";
	
	@Override
	public Boolean updateAProfile(ProfileDetailsDto profileDetailsDto) throws SQLException {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		Boolean status = Boolean.valueOf(false);
		try{
			conn = this.dataSource.getConnection();
			int col = 1;
			ps = conn.prepareStatement(UPDATE_PROFILE_DETAILS);
			ps.setString(col++, profileDetailsDto.getFirstName());
		    ps.setString(col++, profileDetailsDto.getMiddleName());
		    ps.setString(col++, profileDetailsDto.getLastName());
		    ps.setString(col++, profileDetailsDto.getEmailId());
		    ps.setString(col++, profileDetailsDto.getDateOfBirth());
		    ps.setString(col++, profileDetailsDto.getSex().getValue());
		    ps.setString(col++, profileDetailsDto.getAddressStreetName());
		    ps.setString(col++, profileDetailsDto.getCityZipcode());
		    ps.setString(col++, profileDetailsDto.getIsPushNotificationEnabled().getValue());
		    ps.setString(col++, profileDetailsDto.getIsEmailNotificationEnabled().getValue());
		    ps.setString(col++, profileDetailsDto.getActiveStatus().getValue());
		    ps.setTimestamp(col++, DateUtility.getCurrentTimestamp());
		    ps.setString(col++, LithoserUtil.getContactNumber(profileDetailsDto.getPhoneNumber()));
		    ps.setString(col++, profileDetailsDto.getProfileType().getValue());
		    int updateCount = ps.executeUpdate();
		    if (updateCount != 1) {
		    	throw new SQLException("Update to PROFILE_DETAILS was not successful.");
		    }
		    status = Boolean.valueOf(true);
		} finally {
			LithoserUtil.cleanUpSQLData(ps, rs, conn, this);
		}
		return status;
	}

	private static final String INSERT_BATTERY_SALE_DETAILS = 
			"INSERT INTO BATTERY_SALE_DETAIL\r\n" + 
				"(\r\n" + 
				"BATTERY_NAME,\r\n" + 
				"BATTERY_MANUFACTURING_DATE,\r\n" + 
				"BATTERY_IMAGE,\r\n" + 
				"SOLD_TO_PROFILE_ID,\r\n" + 
				"SOLD_BY_PROFILE_ID,\r\n" + 
				"NOTIFICATION_CONFIGURATION,\r\n" + 
				"SALES_PRICE,\r\n" + 
				"CUSTOMER_PAID_AMT,\r\n" + 
				"DISCOUNT_AMT,\r\n" + 
				"SALES_COMMENT,\r\n" + 
				"BATTERY_INSTALLED_DATE,\r\n" +
				"LIFE_EXPECTANCY,\r\n" +
				"QUANTITY,\r\n" +
				"BATTERY_MODEL,\r\n" +
				"DATESTAMP)\r\n" + 
			"VALUES\r\n" + 
				"(\r\n" + 
				" ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?\r\n" + 
				")";
	@Override
	public Boolean registerABatterySale(BatterySaleDetailsDto batterySaleDetailsDto) throws SQLException {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		Boolean status = Boolean.valueOf(false);
		try{
			conn = this.dataSource.getConnection();
			int col = 1;
			ps = conn.prepareStatement(INSERT_BATTERY_SALE_DETAILS);
			ps.setString(col++, batterySaleDetailsDto.getBatteryName());
		    ps.setTimestamp(col++, batterySaleDetailsDto.getBatteryManufacturingDate());
		    ps.setString(col++, batterySaleDetailsDto.getBatteryImage());
		    ps.setLong(col++, LithoserUtil.convertToNumber(batterySaleDetailsDto.getSoldToProfileId(), Long.class).longValue());
		    ps.setLong(col++, LithoserUtil.convertToNumber(batterySaleDetailsDto.getSoldByProfileId(), Long.class).longValue());
		    ps.setInt(col++, LithoserUtil.convertToNumber(batterySaleDetailsDto.getNotificationConfiguration(), Integer.class).intValue());
		    ps.setBigDecimal(col++, batterySaleDetailsDto.getSalesPrice());
		    ps.setBigDecimal(col++, batterySaleDetailsDto.getCustomerPaidAmt());
		    ps.setBigDecimal(col++, batterySaleDetailsDto.getDiscountAmt());
		    ps.setString(col++, batterySaleDetailsDto.getSalesComment());
		    ps.setTimestamp(col++, batterySaleDetailsDto.getInstalledDate());
		    ps.setInt(col++, LithoserUtil.convertToNumber(batterySaleDetailsDto.getLifeExpectancyInDays(), Integer.class).intValue());
		    ps.setInt(col++, LithoserUtil.convertToNumber(batterySaleDetailsDto.getQuantity(), Integer.class).intValue());
		    ps.setString(col++, batterySaleDetailsDto.getBatteryModel());
		    ps.setTimestamp(col++, DateUtility.getCurrentTimestamp());
		    int insertCount = ps.executeUpdate();
		    if (insertCount != 1) {
		    	throw new SQLException("Insert to BATTERY_SALE_DETAILS was not successful.");
		    }
		    status = Boolean.valueOf(true);
		} finally {
			LithoserUtil.cleanUpSQLData(ps, rs, conn, this);
		}
		return status;
	}
	
	private static final String UPDATE_BATTERY_SALE_DETAILS = 
			"UPDATE BATTERY_SALE_DETAIL\r\n" + 
			"SET\r\n" + 
				"BATTERY_NAME = ?,\r\n" + 
				"BATTERY_MANUFACTURING_DATE = ?,\r\n" + 
				"BATTERY_IMAGE = ?,\r\n" + 
				"SOLD_TO_PROFILE_ID = ?,\r\n" + 
				"SOLD_BY_PROFILE_ID = ?,\r\n" + 
				"NOTIFICATION_CONFIGURATION = ?,\r\n" + 
				"SALES_PRICE = ?,\r\n" + 
				"CUSTOMER_PAID_AMT = ?,\r\n" + 
				"DISCOUNT_AMT = ?,\r\n" + 
				"SALES_COMMENT = ?,\r\n" + 
				"BATTERY_INSTALLED_DATE = ?,\r\n" +
				"LIFE_EXPECTANCY = ?,\r\n" +
				"QUANTITY = ?,\r\n" +
				"BATTERY_MODEL = ?,\r\n" +
				"DATESTAMP = ?\r\n" + 
			"WHERE \r\n" + 
				"SALE_DETAILS_ID = ?";
	@Override
	public Boolean updateABatterySale(BatterySaleDetailsDto batterySaleDetailsDto) throws SQLException {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		Boolean status = Boolean.valueOf(false);
		try{
			conn = this.dataSource.getConnection();
			int col = 1;
			ps = conn.prepareStatement(UPDATE_BATTERY_SALE_DETAILS);
			ps.setString(col++, batterySaleDetailsDto.getBatteryName());
		    ps.setTimestamp(col++, batterySaleDetailsDto.getBatteryManufacturingDate());
		    ps.setString(col++, batterySaleDetailsDto.getBatteryImage());
		    ps.setLong(col++, LithoserUtil.convertToNumber(batterySaleDetailsDto.getSoldToProfileId(), Long.class).longValue());
		    ps.setLong(col++, LithoserUtil.convertToNumber(batterySaleDetailsDto.getSoldByProfileId(), Long.class).longValue());
		    ps.setInt(col++, LithoserUtil.convertToNumber(batterySaleDetailsDto.getNotificationConfiguration(), Integer.class).intValue());
		    ps.setBigDecimal(col++, batterySaleDetailsDto.getSalesPrice());
		    ps.setBigDecimal(col++, batterySaleDetailsDto.getCustomerPaidAmt());
		    ps.setBigDecimal(col++, batterySaleDetailsDto.getDiscountAmt());
		    ps.setString(col++, batterySaleDetailsDto.getSalesComment());
		    ps.setTimestamp(col++, batterySaleDetailsDto.getInstalledDate());
		    ps.setInt(col++, LithoserUtil.convertToNumber(batterySaleDetailsDto.getLifeExpectancyInDays(), Integer.class).intValue());
		    ps.setInt(col++, LithoserUtil.convertToNumber(batterySaleDetailsDto.getQuantity(), Integer.class).intValue());
		    ps.setString(col++, batterySaleDetailsDto.getBatteryModel());
		    ps.setTimestamp(col++, DateUtility.getCurrentTimestamp());
		    ps.setLong(col++, LithoserUtil.convertToNumber(batterySaleDetailsDto.getSaleDetailsId(), Long.class).longValue());
		    int updateCount = ps.executeUpdate();
		    if (updateCount != 1) {
		    	throw new SQLException("Update to BATTERY_SALE_DETAILS was not successful.");
		    }
		    status = Boolean.valueOf(true);
		} finally {
			LithoserUtil.cleanUpSQLData(ps, rs, conn, this);
		}
		return status;
	}

	
	private static final String UPDATE_ACTIVE_STATUS_BATTERY_SALE_DETAILS = 
			"UPDATE BATTERY_SALE_DETAIL " + 
			"SET " + 
				"ACTIVE_STATUS = ?, " + 
				"DATESTAMP = ? " +
			"WHERE  " + 
				"SALE_DETAILS_ID = ?";
	@Override
	public Boolean updateActiveStatusOfBatterySale(String activeStatus, Long saleDetailId) throws SQLException {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		Boolean status = Boolean.valueOf(false);
		try{
			conn = this.dataSource.getConnection();
			int col = 1;
			ps = conn.prepareStatement(UPDATE_ACTIVE_STATUS_BATTERY_SALE_DETAILS);
		    ps.setString(col++, activeStatus);
		    ps.setTimestamp(col++, DateUtility.getCurrentTimestamp());
			ps.setLong(col++, saleDetailId);
		    int updateCount = ps.executeUpdate();
		    if (updateCount != 1) {
		    	throw new SQLException("Update activeStatus to BATTERY_SALE_DETAILS was not successful.");
		    }
		    status = Boolean.valueOf(true);
		} finally {
			LithoserUtil.cleanUpSQLData(ps, rs, conn, this);
		}
		return status;
	}
	private static final String GET_BATTERY_SALE_DETAILS = 
			"SELECT\r\n" + 
				"SALE_DETAILS_ID,\r\n" + 
				"BATTERY_NAME,\r\n" + 
				"BATTERY_MANUFACTURING_DATE,\r\n" + 
				"BATTERY_IMAGE,\r\n" + 
				"SOLD_TO_PROFILE_ID,\r\n" + 
				"SOLD_BY_PROFILE_ID,\r\n" + 
				"NOTIFICATION_CONFIGURATION,\r\n" + 
				"SALES_PRICE,\r\n" + 
				"CUSTOMER_PAID_AMT,\r\n" + 
				"DISCOUNT_AMT,\r\n" + 
				"SALES_COMMENT,\r\n" + 
				"BATTERY_INSTALLED_DATE,\r\n" +
				"LIFE_EXPECTANCY,\r\n" +
				"QUANTITY,\r\n" +
				"BATTERY_MODEL,\r\n" +
				"DATESTAMP,\r\n" + 
				"(SELECT PHONE_NUMBER FROM PROFILE_DETAIL WHERE PROFILE_ID = SOLD_TO_PROFILE_ID) CUSTOMER_PHONE_NUMBER\r\n" + 
			"FROM BATTERY_SALE_DETAIL\r\n";
	private static final String WHERE_CLAUSE_BATTERY_SALE_DETAILS_BY_SALE_ID =
			" WHERE SALE_DETAILS_ID = ? AND ACTIVE_STATUS = 'Y'";
	@Override
	public BatterySaleDetailsDto getABatterySaleDetailsBySaleId(Long batterySaleDetailsId) throws SQLException {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		BatterySaleDetailsDto batterySaleDetailsDto = new BatterySaleDetailsDto();
		try {
			conn = this.dataSource.getConnection();

			ps = conn.prepareStatement(GET_BATTERY_SALE_DETAILS
					+ WHERE_CLAUSE_BATTERY_SALE_DETAILS_BY_SALE_ID);
			ps.setLong(1, batterySaleDetailsId.longValue());
			rs = ps.executeQuery();
			if(rs.next()) {
				mapBatterySaleDetails(rs, batterySaleDetailsDto);			
			}
		} finally {
			LithoserUtil.cleanUpSQLData(ps, rs, conn, this);
		}
		return batterySaleDetailsDto;
	}

	private void mapBatterySaleDetails(ResultSet rs, BatterySaleDetailsDto batterySaleDetailsDto) throws SQLException {
		batterySaleDetailsDto.setSaleDetailsId(rs.getLong("SALE_DETAILS_ID"));
		batterySaleDetailsDto.setBatteryName(rs.getString("BATTERY_NAME"));
		batterySaleDetailsDto.setBatteryManufacturingDate(rs.getTimestamp("BATTERY_MANUFACTURING_DATE"));
		batterySaleDetailsDto.setBatteryImage(rs.getString("BATTERY_IMAGE"));
		batterySaleDetailsDto.setSoldToProfileId(rs.getLong("SOLD_TO_PROFILE_ID"));
		batterySaleDetailsDto.setSoldByProfileId(rs.getLong("SOLD_BY_PROFILE_ID"));
		batterySaleDetailsDto.setNotificationConfiguration(rs.getInt("NOTIFICATION_CONFIGURATION"));
		batterySaleDetailsDto.setSalesPrice(rs.getBigDecimal("SALES_PRICE"));
		batterySaleDetailsDto.setCustomerPaidAmt(rs.getBigDecimal("CUSTOMER_PAID_AMT"));
		batterySaleDetailsDto.setDiscountAmt(rs.getBigDecimal("DISCOUNT_AMT"));
		batterySaleDetailsDto.setSaleDate(rs.getTimestamp("DATESTAMP"));
		batterySaleDetailsDto.setSalesComment(rs.getString("SALES_COMMENT"));
		batterySaleDetailsDto.setQuantity(rs.getInt("QUANTITY"));
		batterySaleDetailsDto.setLifeExpectancyInDays(rs.getInt("LIFE_EXPECTANCY"));
		batterySaleDetailsDto.setInstalledDate(rs.getTimestamp("BATTERY_INSTALLED_DATE"));
		batterySaleDetailsDto.setBatteryModel(rs.getString("BATTERY_MODEL"));
		batterySaleDetailsDto.setCustomerPhoneNumber(rs.getString("CUSTOMER_PHONE_NUMBER"));
	}

	private static final String WHERE_CLAUSE_BATTERY_SALE_DETAILS_BY_SELLER_PROFILE_ID =
			" WHERE SOLD_BY_PROFILE_ID = ? AND ACTIVE_STATUS = 'Y'";
	
	private static final String WHERE_CLAUSE_BATTERY_SALE_DETAILS_BY_MACHINE_BATTERY_NAME =
			" AND BATTERY_NAME LIKE ? ";
	@Override
	public List<BatterySaleDetailsDto> getListOfBatterySaleDetailsBySeller(Long sellerProfileId, String machineBatteryName) throws SQLException {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		ArrayList<BatterySaleDetailsDto> batterySaleDetailsList = new ArrayList<BatterySaleDetailsDto>();
		BatterySaleDetailsDto batterySaleDetailsDto = null;
		String query = GET_BATTERY_SALE_DETAILS
				+ WHERE_CLAUSE_BATTERY_SALE_DETAILS_BY_SELLER_PROFILE_ID;
		if(machineBatteryName != null && !machineBatteryName.isEmpty()) {
			query+=WHERE_CLAUSE_BATTERY_SALE_DETAILS_BY_MACHINE_BATTERY_NAME;
		}
		try {
			conn = this.dataSource.getConnection();

			ps = conn.prepareStatement(query);
			ps.setLong(1, sellerProfileId.longValue());
			if(machineBatteryName != null && !machineBatteryName.isEmpty()) {
				query+=WHERE_CLAUSE_BATTERY_SALE_DETAILS_BY_MACHINE_BATTERY_NAME;
				ps.setString(2, machineBatteryName+"%");
			}
			rs = ps.executeQuery();
			while(rs.next()) {
				batterySaleDetailsDto = new BatterySaleDetailsDto();
				mapBatterySaleDetails(rs, batterySaleDetailsDto);
				batterySaleDetailsList.add(batterySaleDetailsDto);
			}
		} finally {
			LithoserUtil.cleanUpSQLData(ps, rs, conn, this);
		}
		return batterySaleDetailsList;
	}

	private static final String WHERE_CLAUSE_BATTERY_SALE_DETAILS_BY_SELLER_BUYER_ID =
			" WHERE SOLD_BY_PROFILE_ID = ? AND SOLD_TO_PROFILE_ID = ? ";
	@Override
	public List<BatterySaleDetailsDto> getListOfBatterySaleDetailsBySellerAndBuyer(Long sellerProfileId,
			Long buyerProfileId) throws SQLException {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		ArrayList<BatterySaleDetailsDto> batterySaleDetailsList = new ArrayList<BatterySaleDetailsDto>();
		BatterySaleDetailsDto batterySaleDetailsDto = null;
		try {
			conn = this.dataSource.getConnection();

			ps = conn.prepareStatement(GET_BATTERY_SALE_DETAILS
					+ WHERE_CLAUSE_BATTERY_SALE_DETAILS_BY_SELLER_BUYER_ID);
			ps.setLong(1, sellerProfileId.longValue());
			ps.setLong(2, buyerProfileId.longValue());
			rs = ps.executeQuery();
			while(rs.next()) {
				batterySaleDetailsDto = new BatterySaleDetailsDto();
				mapBatterySaleDetails(rs, batterySaleDetailsDto);
				batterySaleDetailsList.add(batterySaleDetailsDto);
			}
		} finally {
			LithoserUtil.cleanUpSQLData(ps, rs, conn, this);
		}
		return batterySaleDetailsList;
	}

	private static final String GET_CITY_INFO = " SELECT CITY, STATE, ZIPCODE, LATITUDE, LONGITUDE " + 
												" FROM USA_CITIES_ZIPCODE" + 
												" WHERE ( UPPER(CITY) LIKE UPPER(?) " +
												" AND UPPER(STATE) LIKE UPPER(?) ) " +
												" OR ZIPCODE = ? ";
	/**
	 * fetch the city information when given a city name or zipcode
	 * 
	 * @param name
	 *            String
	 * @param stateCode
	 *            String
	 * @param zipcode
	 *            String            
	 * @return CityInfo
	 */
	@Override
	public CityInfo getCityInformation(String name, String stateCode, String zipcode) throws SQLException {
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		CityInfo cityInfo = new CityInfo();
		try {
			conn = this.dataSource.getConnection();

			ps = conn.prepareStatement(GET_CITY_INFO);
			int parameterIndex = 1;
			ps.setString(parameterIndex++, name);
			ps.setString(parameterIndex++, stateCode);
			ps.setString(parameterIndex++, zipcode);
			rs = ps.executeQuery();
			if(rs.next()) {
				cityInfo.setName(rs.getString("city"));
				cityInfo.setZipcode(rs.getString("zipcode"));
				cityInfo.setStateCode(rs.getString("state"));
				cityInfo.setLatitude(rs.getDouble("latitude"));
				cityInfo.setLongitude(rs.getDouble("longitude"));
			}
		} finally {
			LithoserUtil.cleanUpSQLData(ps, rs, conn, this);
		}
		return cityInfo;
	}
}
